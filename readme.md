## Windows 10 with Edge browser, one ```vagrant up``` away.

Need to test something on Windows 10? No need to go begging your IT stuff for a spare license key. Microsoft already provides ready-made images you can slurp in.

## TODO

1. The rdesktop command has no configuration file, so for now using ```alias rdesktop='rdesktop -g 60%'``` should do the trick.
2. Auto-snapshoting on first run
